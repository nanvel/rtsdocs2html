#!/usr/bin/python

import sys, os

from docutils import core


def rts2html(path):
    source = open(path, 'r')
    path_dest = path[:-4] + '.html'
    destination = open(path_dest, 'w')
    core.publish_file(source=source,
                        destination=destination,
                        writer_name='html')
    source.close()
    destination.close()


if __name__ == "__main__":
    '''
    Input params:
        - Absolute path to docs directory (not required. If not specified,
        will try ./docs and ./../docs)
    '''
    folders = ['./docs', '../docs']
    if len(sys.argv) == 2:
        folders.insert(0, sys.argv[1])
    for folder in folders:
        if os.path.isdir(folder):
            for root, dirnames, filenames in os.walk(folder):
                for fn in filenames:
                    if len(fn) < 5 or fn[-4:].lower() != '.rst':
                        continue
                    path = os.path.join(root, fn)
                    try:
                        rts2html(path)
                        print 'Converted %s' % fn
                    except Exception as e:
                        print 'Error while convering %s: %s' % (fn,
                                                                str(e))
            break
